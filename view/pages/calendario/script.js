document.addEventListener("DOMContentLoaded", function () {
  var calendarEl = document.getElementById("calendar");

  var calendar = new FullCalendar.Calendar(calendarEl, {
    initialView: "dayGridMonth",
    events: [
      {
        title: "Evento 1",
        start: "2023-10-01",
      },
      {
        title: "Evento 2",
        start: "2023-10-05",
        end: "2023-10-07",
      },
      {
        title: "Evento 3",
        start: "2023-10-09T12:30:00",
      },
    ],
  });

  calendar.render();
});
