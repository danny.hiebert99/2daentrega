<div class="">
  <div class="flex justify-evenly items-center sm:gap-6 md:gap-12">
    <button class="group text-bordo relative inline-block hover:text-rose-600">
      Conocenos
      <span
        class="absolute bottom-0 left-1/2 transform -translate-x-1/2 w-0 h-[2px] bg-rose-600 transition-width duration-300 ease-in-out group-hover:w-full "></span>
    </button>
    <button class="group text-bordo relative inline-block hover:text-rose-600">
      Loteamientos
      <span
        class="absolute bottom-0 left-1/2 transform -translate-x-1/2 w-0 h-[2px] bg-rose-600 transition-width duration-300 ease-in-out group-hover:w-full "></span>
    </button>
    <button class="group text-bordo relative inline-block hover:text-rose-600">
      Pirarenda
      <span
        class="before:[&>a]:absolute before:[&>a]:bottom-0 before:[&>a]:left-1/2 before:[&>a]:transform before:[&>a]:-translate-x-1/2 before:[&>a]:w-0 before:[&>a]:h-[2px] before:[&>a]:bg-rose-600 before:[&>a]:transition-width before:[&>a]:duration-300 before:[&>a]:ease-in-out before:[&>a]:group-hover:w-full "></span>
    </button>
    <button class="group text-bordo relative inline-block hover:text-rose-600">
      Contactanos
      <span
        class="absolute bottom-0 left-1/2 transform -translate-x-1/2 w-0 h-[2px] bg-rose-600 transition-width duration-300 ease-in-out group-hover:w-full "></span>
    </button>
  </div>
</div>