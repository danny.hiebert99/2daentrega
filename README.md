#### Universidad Católica Nuestra Señora de la Asunción
#### Facultad de Ciencia y Tecnología – FCyT
#### Departamento de Análisis de Sistema - DAS

# Ingeniería del Software 4 - 2023

## Trabajo Práctico de Ingeniería del Software 4

### Alumnos: Danny Hiebert, Sebastian Bogado, Nahuel Aguero

### Requisitos de segunda entrega

### Documento de Diseño – V2

## Datos Generales:
- Carrera: Análisis de Sistemas
- Asignatura: Ingeniería del Software 4
- Responsables: Ing. Mauricio Merín, Lic. Omar Esgaib

## Datos de Entrega:
- Fecha de entrega: lunes 23 de octubre del 2022
- Horario: En el horario de clase (de 18 a 22 horas)
- Defensa: Se requiere defensa de la entrega – Deberán estar todos los integrantes de cada grupo.

## Lineamientos generales:
En base al tema elegido y a las funcionalidades presentadas en la primera entrega se realizará el documento de diseño.

# Requisitos de la entrega (Puntaje Total Asignado a la entrega: 4 puntos):
## Se pide entregar la siguiente información:
- Diseño del Esquema lógico de la BD, Diseño del Esquema físico de la BD y Script de creación de Base de datos (para PostgreSQL 9.6 o superior) – **Puntaje asignado 1 punto.**
- Layout del sistema (se entrega el html, css y Javascript en formato digital) – **Puntaje asignado 1 punto.**
- MockUP de las principales interfaces (definir 5 interfaces principales que tendrá el sistema) – **Puntaje asignado: 0,5 puntos.**
- Definición de estándares y convenciones de grupo (estándares de documentación, estándares de codificación, modelo de proceso seguido, arquitectura definida para el sistema). También se debe definir arquitectura de la aplicación. – **Puntaje asignado: 0,5 puntos.**
- Definición de todas las herramientas que utilizará el grupo (IDE, cliente de BD, sistema de versionamiento, sistema de tracking, herramientas wiki, herramientas de documentación, entre otras). – **Puntaje asignado: 0,5 puntos.**
- Diagramas UML (Diagrama de Casos de Uso y Diagrama de Despliegue) – Cualquier otro diagrama que el grupo considere necesario. – **Puntaje asignado: 0,5 punto.**
Observación: Se deben incluir todas las mejoras de la primera entrega.
Formato de entrega:
Formato Digital - El documento se deberá levantar a las plataformas del curso dentro del horario establecido. Aclaración: No se reciben documentos fuera de la fecha estipulada.